/* 1. У JavaScript об'єкти мають спеціальну приховану властивість Prototype,
яка або дорівнює null або посилається на інший об'єкт. Цей об'єкт 
називається прототипом. Коли ми хочемо прочитати властивість з object,
а вона відсутня, JavaScript автоматично бере його з прототипу, це і
є прототипним наслідуванням.

2. Викликати super() у конструкторі класу-нащадка потрібно щоб
розширити клас за допомогою наслідування, або щоб 
використати чи модифікувати поведінку батьківського класу.
*/
"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        return this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        return this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        return this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        return this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const monicaGeller = new Programmer('Monica Geller', 33, 3000, 'Pyton');
console.log(monicaGeller);

const joeyTribbiani = new Programmer('Joey Tribbiani', 40, 3600, 'Java-Script');
console.log(joeyTribbiani);
console.log(joeyTribbiani.lang);

const сhandlerBing = new Programmer('Chandler Muriel Bing', 54, 4000, 'Java');
console.log(сhandlerBing);
console.log(сhandlerBing.salary);