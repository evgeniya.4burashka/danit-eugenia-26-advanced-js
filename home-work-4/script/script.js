/**AJAX - це технологія, яка дозволяє здійснювати асинхронний обмін даними 
 * між клієнтом та сервером без потреби перезавантажувати сторінку. 
 * Завдяки AJAX можна оновлювати окремі частини сторінки безпосередньо на клієнті.*/

"use strict"

const root = document.querySelector("#root");

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {

        data.forEach(({ episodeId, name, characters, openingCrawl }) => {

            const innerBlock = document.createElement("div");
            root.append(innerBlock);

            innerBlock.insertAdjacentHTML("beforeend",
                `<p style="font-size: 25px;">EPISODE: ${episodeId}</p>
                 <h3>NAME: ${name}</h3>
                 <div class="persons">CHARACTERS: </div>
                 <p>BRIEF DESCRIPTION OF THE MOVIE: ${openingCrawl}</p>`)

            const persons = innerBlock.querySelector(".persons");

            characters.forEach((character) => {
                fetch(character)
                    .then(response => response.json())
                    .then((character) => {

                        persons.insertAdjacentHTML("beforeend",
                            `<li>${character.name}</li>`);
                    })
            })
        })
    })
    .catch(error => console.error('Помилка отримання даних:', error));