const root = document.querySelector('#root');

const innerBlock = document.createElement("div");
root.append(innerBlock);

const URL_users = "https://ajax.test-danit.com/api/json/users";
const URL_posts = "https://ajax.test-danit.com/api/json/posts";

class Card {
    constructor(user, post) {
        this.user = user;
        this.post = post;
    }

    renderCards() {
        innerBlock.insertAdjacentHTML("beforeend",
            `<div class="cards" data-id="${this.post.id}">
            <div class="name-email-button">
            <div class="name-email">
             <h3> ${this.user.name} </h3> 
             <span> ${this.user.email}</span> 
             </div>
           <div>
             <button class="button">❌</button>
             </div>
             </div>
             <h4> ${this.post.title}</h4>
             <p> ${this.post.body}</p>
             </div>`)
    }
}

Promise.all([
    fetch(URL_users).then((response) => response.json()),
    fetch(URL_posts).then((response) => response.json()),
]).then(([usersArr, postsArr]) => {
    postsArr.forEach((posts) => {
        const users = usersArr.find((user) => user.id === posts.userId);
        const card = new Card(users, posts);
        card.renderCards();

    })

    const buttons = innerBlock.querySelectorAll('.button');
    buttons.forEach(button => {
        button.addEventListener('click', async (event) => {
            const card = button.closest('.cards');
            const postId = card.getAttribute('data-id');

            try {
                const response = await fetch(URL_posts + "/" + postId, {
                    method: "DELETE",
                });

                if (response.ok) {
                    card.remove();
                    console.log(`Пост ${postId} видалено`);
                }
            } catch (error) {
                console.error(error);
            }
        });
    });

}).catch((err) => console.log(err));