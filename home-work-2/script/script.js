/* Конструкція try...catch використовується для обробки
 помилок під час виконання програми. 
 При отриманні даних зовнішнього джерела, 
 такого як API запит, файл або вхідні дані від користувача, 
 виникає ризик непередбачених помилок. try...catch дозволяє обробляти 
 ці помилки без зупинки виконання програми.
 Операції з DOM можуть викидати виключення, наприклад якщо спробувати 
 отримати доступ до неіснуючого елемента. При використанні асинхронних операцій, 
 таких як setTimeout або fetch, виникає потреба у відловленні помилок, якщо операція 
 завершиться невдало. Парсинг JSON даних може призвести до помилок, наприклад, якщо 
 дані не є валідним JSON.Завжди важливо правильно обробляти помилки, щоб забезпечити 
 стабільність та безпеку коду.
 */

"use strict"

const root = document.querySelector("#root");

const ul = document.createElement("ul");
root.appendChild(ul);

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


books.forEach(function (element) {
    try {
        if (element.author === undefined || element.name === undefined || element.price === undefined) {
            throw error;
        } else {
            ul.insertAdjacentHTML("beforeend", 
            `<li>AUTHOR ${element.author},
            NAME ${element.name},
            PRISE ${element.price}$</li>`)
        }
    } catch (error) {
    if (element.author === undefined || element.name === undefined || element.price === undefined) {
        console.warn(`AUTHOR ${element.author},
        NAME ${element.name},
        PRISE ${element.price}$`);
    }
    }
});