/**Деструктуризація це синтаксичний підхід, який дозволяє виділяти окремі елементи з об’єктів або масивів
 * і присвоювати їх змінним. Вона дозволяє зручно отримувати значення зі складних структур даних.
 * Вона дозволяє зробити код більш зрозумілим, скоротити кількість зайвих змінних та полегшити 
 * роботу зі структурами даних. */

"use strict"


// 1 Завдвння
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsOl = [...new Set([...clients1, ...clients2])];
console.log(clientsOl);


// 2 Завдання
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map(({ name, lastName, age }) => ({
    name,
    lastName,
    age
}));

console.log(charactersShortInfo);


// 3 Завдання
const user1 = {
    name: "John",
    years: 30
};

const { name, years, isAdmin = false } = user1;
console.log(name, years, isAdmin);


// 4 Завдання
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = Object.assign(satoshi2018, satoshi2019, satoshi2020);
console.log(fullProfile);

// 5 Завдання
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const changedBooks = [...books, {...bookToAdd}];
console.log(changedBooks);


// 6 Завдання
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

  const changedEmployee = {...employee, age: 30, salary: 2000}
console.log(changedEmployee);


// 7 завдання
const array = ['value', () => 'showValue'];

// Допишіть код тут
const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'